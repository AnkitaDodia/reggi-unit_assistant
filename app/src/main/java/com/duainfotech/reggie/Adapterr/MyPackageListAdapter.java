package com.duainfotech.reggie.Adapterr;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.duainfotech.reggie.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by duainfotech on 1/10/17.
 */
public class MyPackageListAdapter extends BaseAdapter {

    ArrayList<HashMap<String, String>> PackageDetailss;
    Context ctx;
    LayoutInflater l_Inflater;

    public MyPackageListAdapter(Context ctx, ArrayList<HashMap<String, String>> eventsDetails) {

        PackageDetailss = eventsDetails;
        this.ctx = ctx;
        l_Inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return PackageDetailss.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View convertView = view;
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();

            convertView = l_Inflater.inflate(R.layout.mypackagelistrow, null);
            holder.txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
            holder.txtValue = (TextView) convertView.findViewById(R.id.txtValue);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtTitle.setText(PackageDetailss.get(i).get("Header"));
        holder.txtValue.setText(PackageDetailss.get(i).get("Value"));

        return convertView;
    }

    public static class ViewHolder {
        TextView txtTitle,txtValue;
    }
}
