package com.duainfotech.reggie.Adapterr;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.duainfotech.reggie.Activityy.UpdateNewsLetterActivity;
import com.duainfotech.reggie.R;
import com.duainfotech.reggie.Utils.UtilsFile;

import java.util.ArrayList;


public class PdfListAdapter extends BaseAdapter {

    ArrayList<String> FileName = new ArrayList<String>();
    ArrayList<String> FilePath = new ArrayList<String>();

    Context ctx;
    LayoutInflater l_Inflater;

    public PdfListAdapter(Context ctx, ArrayList<String> pdfname, ArrayList<String> pdfpath) {

        this.ctx = ctx;
        FileName = pdfname;
        FilePath = pdfpath;
        l_Inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return FilePath.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View inView, ViewGroup arg2) {
        View convertView = inView;
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();

            convertView = l_Inflater.inflate(R.layout.pdflistrow, null);
            holder.lblTopicName = (TextView) convertView.findViewById(R.id.txtName);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.lblTopicName.setText(FileName.get(position));

        convertView.setClickable(true);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ctx, FilePath.get(position), Toast.LENGTH_LONG).show();
                UtilsFile.PdfUri = Uri.parse(FilePath.get(position));
                UpdateNewsLetterActivity.checkfile = true;
                UpdateNewsLetterActivity.imageorpdf = "pdf";
                ((Activity) ctx).finish();
            }
        });

        return convertView;
    }

    public static class ViewHolder {
        TextView lblTopicName;
    }
}