package com.duainfotech.reggie.Activityy;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.duainfotech.reggie.R;

public class NewslatterActivity extends AppCompatActivity implements View.OnClickListener {

    WebView webview;
    ProgressBar progress;
    RelativeLayout btnBack,btnShare;
    String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_newslatter);

        webview = (WebView) findViewById(R.id.webview);
        progress = (ProgressBar) findViewById(R.id.progressSocial);
        btnBack = (RelativeLayout) findViewById(R.id.btnBack);
        btnShare = (RelativeLayout) findViewById(R.id.btnShare);

        Bundle bundle = null;
        bundle = this.getIntent().getExtras();
        String url = bundle.getString("URL");

        Log.e("URL Before", url);
        link = url;

//        if (url.contains(".pdf")) {
            url = "http://docs.google.com/gview?embedded=true&url="+url;
//        }
        Log.e("URL After", url);

        webview.setWebViewClient(new myBrowser());

        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setLoadsImagesAutomatically(true);
        webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        if (hasConnection(this)) {
            webview.setVisibility(View.VISIBLE);
            progress.setVisibility(View.VISIBLE);
            webview.loadUrl(url);
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection..", Toast.LENGTH_LONG).show();
        }

        btnBack.setOnClickListener(this);
        btnShare.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
            case R.id.btnShare:
                shareLink();
                break;
        }
    }

    private void shareLink()
    {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        share.putExtra(Intent.EXTRA_TEXT,link);

        startActivity(Intent.createChooser(share, "Share link!"));
    }

    private class myBrowser extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            progress.setVisibility(View.VISIBLE);

            view.loadUrl(url);
            return true;
        }

        /**
         * Notify the host application that an SSL error occurred while loading a
         * resource. The host application must call either handler.cancel() or
         * handler.proceed(). Note that the decision may be retained for use in
         * response to future SSL errors. The default behavior is to cancel the
         * load.
         *
         * @param view    The WebView that is initiating the callback.
         * @param handler An SslErrorHandler object that will handle the user's
         *                response.
         * @param error   The SSL error object.
         */
        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            //final AlertDialog.Builder builder = new AlertDialog.Builder(OnlinePayment.this);
            String msg="";
            if(error.getPrimaryError()==SslError.SSL_DATE_INVALID
                    || error.getPrimaryError()== SslError.SSL_EXPIRED
                    || error.getPrimaryError()== SslError.SSL_IDMISMATCH
                    || error.getPrimaryError()== SslError.SSL_INVALID
                    || error.getPrimaryError()== SslError.SSL_NOTYETVALID
                    || error.getPrimaryError()==SslError.SSL_UNTRUSTED) {
                if(error.getPrimaryError()==SslError.SSL_DATE_INVALID){
                    msg="The date of the certificate is invalid";
                }else if(error.getPrimaryError()==SslError.SSL_INVALID){
                    msg="A generic error occurred";
                }
                else if(error.getPrimaryError()== SslError.SSL_EXPIRED){
                    msg="The certificate has expired";
                }else if(error.getPrimaryError()== SslError.SSL_IDMISMATCH){
                    msg="Hostname mismatch";
                }
                else if(error.getPrimaryError()== SslError.SSL_NOTYETVALID){
                    msg="The certificate is not yet valid";
                }
                else if(error.getPrimaryError()==SslError.SSL_UNTRUSTED){
                    msg="The certificate authority is not trusted";
                }
            }
            final AlertDialog.Builder builder = new AlertDialog.Builder(NewslatterActivity.this);
            builder.setMessage(msg);
            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);

            progress.setVisibility(View.GONE);
        }
    }

    public static boolean hasConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }

        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
