package com.duainfotech.reggie.Activityy;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.duainfotech.reggie.R;
import com.duainfotech.reggie.Utils.Api;
import com.duainfotech.reggie.Utils.CustomRequest;
import com.yinglan.keyboard.HideUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ContactMainActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout btnBack, btnSave;
    EditText txtemail, txtcontact, txtname, txtSubject, txtMessage;

    TextView lblname, lblemail, lblcontact, lblSubject, lblMessage,lblNote;
    public static ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_contact_main);

        HideUtil.init(this);

        btnBack = (RelativeLayout) findViewById(R.id.btnBack);
        btnSave = (RelativeLayout) findViewById(R.id.btnSave);
        txtemail = (EditText) findViewById(R.id.txtemail);
        txtcontact = (EditText) findViewById(R.id.txtcontact);
        txtname = (EditText) findViewById(R.id.txtname);
        txtSubject = (EditText) findViewById(R.id.txtSubject);
        txtMessage = (EditText) findViewById(R.id.txtMessage);

        lblname = (TextView) findViewById(R.id.lblname);
        lblemail = (TextView) findViewById(R.id.lblemail);
        lblcontact = (TextView) findViewById(R.id.lblcontact);
        lblSubject = (TextView) findViewById(R.id.lblSubject);
        lblMessage = (TextView) findViewById(R.id.lblMessage);
        lblNote = (TextView) findViewById(R.id.lblNote);

        String notee = "<font color=#FF0000>*</font> <font color=#000000> Indicates mandatory fields.</font>";
        lblNote.setText(Html.fromHtml(notee));

        String name = "<font color=#000000>Name </font> <font color=#FF0000>*</font> <font color=#000000>:</font>";
        lblname.setText(Html.fromHtml(name));

        String email = "<font color=#000000>Email </font> <font color=#FF0000>*</font> <font color=#000000>:</font>";
        lblemail.setText(Html.fromHtml(email));

        String contact = "<font color=#000000>Contact No </font> <font color=#FF0000>*</font> <font color=#000000>:</font>";
        lblcontact.setText(Html.fromHtml(contact));

        String subject = "<font color=#000000>Subject </font> <font color=#FF0000>*</font> <font color=#000000>:</font>";
        lblSubject.setText(Html.fromHtml(subject));

        String message = "<font color=#000000>Description </font> <font color=#FF0000>*</font> <font color=#000000>:</font>";
        lblMessage.setText(Html.fromHtml(message));


        progressDialog = new ProgressDialog(ContactMainActivity.this);
        progressDialog.setMessage("Loading...!");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        btnBack.setOnClickListener(this);
        btnSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
            case R.id.btnSave:
                contactapicall();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(ContactMainActivity.this, HomeActivity.class));
        finish();
    }

    public void opedialog(String title, String value) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ContactMainActivity.this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(value).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
    }

    private void contactapicall() {
        if (CustomRequest.hasConnection(this)) {

            progressDialog.show();

            String api = Api.baseurl + Api.contactapi;

            Map<String, String> params = new HashMap<>();

            params.put("name", txtname.getText().toString());
            params.put("email", txtemail.getText().toString());
            params.put("cell_number", txtcontact.getText().toString());
            params.put("subject", txtSubject.getText().toString());
            params.put("message", txtMessage.getText().toString());

            CustomRequest customRequest = new CustomRequest(Request.Method.POST, api, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("RESPONSE", "count==>" + response.toString());
                    try {

                        String mstatus = response.getString("status");
                        if (mstatus.equals("1")) {

                            progressDialog.dismiss();
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ContactMainActivity.this);
                            alertDialogBuilder.setTitle("Success");
                            alertDialogBuilder.setMessage(response.getString("message")).setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            onBackPressed();
                                        }
                                    }).show();

                        } else {
                            progressDialog.dismiss();
                            opedialog("Failed", response.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("ERROR", "count==>" + error.toString());
                    progressDialog.dismiss();
                }
            }) {
            };
            customRequest.setShouldCache(false);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(customRequest);
        } else {
            progressDialog.dismiss();
            opedialog("Connection Problem", "No Internet Connection !");
        }
    }
}
