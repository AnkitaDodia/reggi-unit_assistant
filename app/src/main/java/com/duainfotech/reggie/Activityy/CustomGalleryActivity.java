package com.duainfotech.reggie.Activityy;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.duainfotech.reggie.R;

import java.io.File;

/**
 * Created by Ankita on 07-Jun-17.
 */
public class CustomGalleryActivity extends MasterActivity
{
    private int count;
    private Bitmap[] thumbnails;
    private boolean[] thumbnailsselection;
    private String[] arrPath;
    private ImageAdapter imageAdapter;

    RelativeLayout btnBack_image;

    GridView imagegrid;

    private Cursor cursor;
    /*
     * Column index for the Thumbnails Image IDs.
     */
    private int columnIndex;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_image_selection);

        btnBack_image = (RelativeLayout) findViewById(R.id.btnBack_image);

        imagegrid = (GridView) findViewById(R.id.PhoneImageGrid);

        try {
            String[] projection = {MediaStore.Images.Thumbnails._ID};
            // Create the cursor pointing to the SDCard
            cursor = managedQuery( MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
                    projection, // Which columns to return
                    null,       // Return all rows
                    null,
                    MediaStore.Images.Thumbnails.IMAGE_ID);
            // Get the column index of the Thumbnails Image ID
            this.count = cursor.getCount();
            this.thumbnails = new Bitmap[this.count];
            this.arrPath = new String[this.count];
            this.thumbnailsselection = new boolean[this.count];
            columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails._ID);
            imagegrid.setAdapter(new ImageAdapter(this));

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        final Button selectBtn = (Button) findViewById(R.id.selectBtn);
        selectBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                final int len = thumbnailsselection.length;
                int cnt = 0;
                String selectImages = "";
                for (int i =0; i<len; i++)
                {
                    if (thumbnailsselection[i]){
                        cnt++;
                        selectImages = selectImages + arrPath[i] + "|";

                        Log.e("SELECTED_IMAGES", "" + selectImages);
                    }
                }

                if (cnt == 0){
                    Toast.makeText(getApplicationContext(),
                            "Please select at least one image",
                            Toast.LENGTH_LONG).show();
                    checkfile = false;
                } else {
                    Log.d("SelectedImages", selectImages);
                    checkfile = true;
                    imageorpdf = "image";
                    setResult(RESULT_OK);
                    finish();
                }
            }
        });

        btnBack_image.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public class ImageAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        CustomGalleryActivity ctx;

        public ImageAdapter(CustomGalleryActivity mCtx) {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            ctx = mCtx;
        }

        public int getCount() {
            return cursor.getCount();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = mInflater.inflate(
                        R.layout.galleryitem, null);
                holder.imageview = (ImageView) convertView.findViewById(R.id.thumbImage);
                holder.checkbox = (CheckBox) convertView.findViewById(R.id.itemCheckBox);

                convertView.setTag(holder);
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.checkbox.setId(position);
            holder.imageview.setId(position);

            holder.checkbox.setOnClickListener(new OnClickListener() {

                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    CheckBox cb = (CheckBox) v;
                    int id = cb.getId();
                    if (thumbnailsselection[id]) {
                        cb.setChecked(false);
                        thumbnailsselection[id] = false;

                        filemapt.remove(position);
                        ImageName.remove(position);
                        Log.e("filemapt", "" + filemapt);
                    } else {
                        cb.setChecked(true);
                        thumbnailsselection[id] = true;
//                            filemapt.add(new File(arrPath[position]));

                        String[] projection = {MediaStore.Images.Media.DATA};
                        cursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                projection, // Which columns to return
                                null,       // Return all rows
                                null,
                                null);
                        columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                        cursor.moveToPosition(position);
                        // Get image filename
                        String imagePath = cursor.getString(columnIndex);
                        filemapt.add(new File(imagePath));
                        Log.e("columnIndex", "" + columnIndex);
                        Log.e("imagePath", "" + imagePath);
                        Log.e("filemapt", "" + filemapt);
                        ImageName.add(imagePath);
                    }
                }
            });

            cursor.moveToPosition(position);
            // Get the current value for the requested column
            int imageID = cursor.getInt(columnIndex);
            // Set the content of the image based on the provided URI
            holder.imageview.setImageURI(Uri.withAppendedPath(
                    MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "" + imageID));

            holder.checkbox.setChecked(thumbnailsselection[position]);
            holder.id = position;
            return convertView;
        }
    }

    class ViewHolder {
        ImageView imageview;
        CheckBox checkbox;
        int id;
    }

    @Override
    public void onBackPressed() {
        finish();
        Toast.makeText(this, "You haven't picked File", Toast.LENGTH_LONG).show();
    }
}
