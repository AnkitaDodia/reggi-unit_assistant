package com.duainfotech.reggie.Activityy;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.duainfotech.reggie.R;
import com.duainfotech.reggie.Utils.Api;
import com.duainfotech.reggie.Utils.CustomRequest;
import com.duainfotech.reggie.Utils.UtilsFile;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout btnNewslatter,btnSpanishNewslatter,btnApproveNewsletter, btnProfile,
            btnUpdateNewslatter, btnLogout, btnMyPackage, btnBack,btnContact,btnLink,btnLink2,btnAppTraining,btn_con_pack_english,
            btn_con_pack_spanish,btn_vip_fb;

    ///////////////Push Notification
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String PROPERTY_REG_ID = "registration_id";

    public static Context con;
    String regid;
    static final String TAG = "REGGIE";
    GoogleCloudMessaging gcm;


    public static ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_home);

        progressDialog = new ProgressDialog(HomeActivity.this);
        progressDialog.setMessage("Please Wait...!");
        progressDialog.setCancelable(false);

        con = this;

        btnBack = (RelativeLayout) findViewById(R.id.btnBack);
        btnProfile = (RelativeLayout) findViewById(R.id.btnProfile);
        btnNewslatter = (RelativeLayout) findViewById(R.id.btnNewslatter);
        btnSpanishNewslatter = (RelativeLayout) findViewById(R.id.btnSpanishNewslatter);
        btnApproveNewsletter = (RelativeLayout) findViewById(R.id.btnApproveNewsletter);
        btnUpdateNewslatter = (RelativeLayout) findViewById(R.id.btnUpdateNewslatter);
        btnLogout = (RelativeLayout) findViewById(R.id.btnLogout);
        btnMyPackage = (RelativeLayout) findViewById(R.id.btnMyPackage);
        btnContact = (RelativeLayout) findViewById(R.id.btnContact);
        btnLink = (RelativeLayout) findViewById(R.id.btnLink);
        btnLink2 = (RelativeLayout) findViewById(R.id.btnLink2);
        btnAppTraining = (RelativeLayout) findViewById(R.id.btnAppTraining);
        btn_con_pack_english = (RelativeLayout) findViewById(R.id.btn_con_pack_english);
        btn_con_pack_spanish = (RelativeLayout) findViewById(R.id.btn_con_pack_spanish);
        btn_vip_fb = findViewById(R.id.btnAppVIPFB);

        btnBack.setOnClickListener(this);
        btnNewslatter.setOnClickListener(this);
        btnSpanishNewslatter.setOnClickListener(this);
        btnApproveNewsletter.setOnClickListener(this);
        btnProfile.setOnClickListener(this);
        btnUpdateNewslatter.setOnClickListener(this);
        btnLogout.setOnClickListener(this);
        btnMyPackage.setOnClickListener(this);
        btnContact.setOnClickListener(this);
        btnLink.setOnClickListener(this);
        btnLink2.setOnClickListener(this);
        btnAppTraining.setOnClickListener(this);
        btn_con_pack_english.setOnClickListener(this);
        btn_con_pack_spanish.setOnClickListener(this);
        btn_vip_fb.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnProfile:
                startActivity(new Intent(HomeActivity.this, ProfileActivity.class));
                break;
            case R.id.btnNewslatter:
                BitlyApiCall("en");
                break;
            case R.id.btnSpanishNewslatter:
                BitlyApiCall("sn");
                break;
            case R.id.btnApproveNewsletter: {
                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                builder.setTitle("Confirmation?");
                builder.setMessage("Are you sure you want to approve your newsletter?");
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        NewsLetterApporvalApiCall();
                    }
                });
                builder.setNegativeButton("NO",null);
                builder.show();
            }
                break;
            case R.id.btnUpdateNewslatter:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.unitassistant.com/newsletteruploads")));
                break;
            case R.id.btnMyPackage:
                startActivity(new Intent(HomeActivity.this, MyPackageActivity.class));
                break;
            case R.id.btnContact:
                startActivity(new Intent(HomeActivity.this, ContactMainActivity.class));
                finish();
                break;
            case R.id.btnLink:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.unitnet.com/ua/#/")));
                break;
            case R.id.btnLink2:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.yourpinkdelivery.com/")));
                break;
            case R.id.btnAppTraining:
                //Old url http://www.ourunitapp.net/_HowTo/web/landing.php
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.unitassistant.com/textblasttraining")));
                break;
            case R.id.btn_con_pack_english:
                callConsultantApi("en");
                break;
            case R.id.btn_con_pack_spanish:
                callConsultantApi("sn");
                break;
            case R.id.btnAppVIPFB:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/groups/unitassistant/")));
                break;
            case R.id.btnLogout:
                startActivity(new Intent(HomeActivity.this, MainActivity.class));
                finish();
                break;
            case R.id.btnBack:
                onBackPressed();
                break;
        }
    }

    private void callConsultantApi(String str)
    {
        if (CustomRequest.hasConnection(this)) {

            String clientId = UtilsFile.clientID;

            progressDialog.show();

            String api = Api.baseurl + Api.consultantPacket;

            Map<String, String> params = new HashMap<>();
            params.put("id_client", clientId);
            params.put("language", str);

            Log.e("Params",params.toString());

            CustomRequest customRequest = new CustomRequest(Request.Method.POST, api, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("RESPONSE", "Consultant packet Response==>" + response.toString());
                    try {

                        if(response.has("status") && response.getString("status").equals("1"))
                        {
                            progressDialog.dismiss();
                            String data = response.getString("data");

                            Uri webpage = Uri.parse(data);

                            if (!data.startsWith("http://") && !data.startsWith("https://")) {
                                webpage = Uri.parse("http://" + data);
                            }

                            startActivity(new Intent(Intent.ACTION_VIEW, webpage));
                        }else {
                            progressDialog.dismiss();
                            opedialog("Failed", response.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                }
            }) {
            };
            customRequest.setShouldCache(false);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(customRequest);
        } else {
            opedialog("Connection Problem", "No Internet Connection !");
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(HomeActivity.this, MainActivity.class));
        finish();
    }

    private void BitlyApiCall(String str)
    {
        if (CustomRequest.hasConnection(this))
        {
            progressDialog.show();

            String api = Api.baseurl + Api.bitlyapi;

            Map<String, String> params = new HashMap<>();
            params.put("id_client", UtilsFile.clientID);
            params.put("bitly", str);

            Log.e("PARAMS",""+params);

            CustomRequest customRequest = new CustomRequest(Request.Method.POST, api, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("RESPONSE", "Bitly Responce==>" + response.toString());
                    try {
                        String mstatus = response.getString("status");
                        if (mstatus.equals("1")) {

                            JSONObject jsonObject = response.getJSONObject("data");

                            String str;

                            if(response.getJSONObject("data").has("en"))
                            {
                                str = jsonObject.getString("en");
                            }
                            else
                            {
                                str = jsonObject.getString("sn");
                            }

                            progressDialog.dismiss();

                            Intent i = new Intent(HomeActivity.this, NewslatterActivity.class);
                            Bundle bundle = new Bundle();
                            Log.e("URL","http://" + str);
                            bundle.putString("URL", "http://" + str);
                            i.putExtras(bundle);
                            startActivity(i);
                        } else {
                            progressDialog.dismiss();
                            opedialog("Failed", response.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    Log.i("ERROR", "count==>" + error.toString());
                    progressDialog.dismiss();
                }
            }) {
            };
            customRequest.setShouldCache(false);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(customRequest);
        } else {
            opedialog("Connection Problem", "No Internet Connection !");
        }
    }

    private String getRegistrationId(Context applicationContext) {
        // TODO Auto-generated method stub
        final SharedPreferences prefs = getGCMPreferences(con);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(getApplicationContext());
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public void opedialog(String title, String value) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(HomeActivity.this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(value).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
    }

    private void NewsLetterApporvalApiCall() {
        if (CustomRequest.hasConnection(this)) {

            progressDialog.show();

            String api = Api.baseurl + Api.approvenewslettereapi;

            Map<String, String> params = new HashMap<>();

            params.put("id_newsletter", UtilsFile.clientID);
            Log.d("Params",UtilsFile.clientID);

            CustomRequest customRequest = new CustomRequest(Request.Method.POST, api, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("RESPONSE", "count==>" + response.toString());
                    progressDialog.dismiss();
                    try {
                        if (response.getInt("status") == 1) {
                            Toast.makeText(HomeActivity.this,response.getString("message"),Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(HomeActivity.this,response.getString("message"),Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("ERROR", "count==>" + error.toString());
                    progressDialog.dismiss();

                    if (error.networkResponse != null) {
                        Log.e("ERROR", "Error Response code: " + error.networkResponse.statusCode);
                    }
                }
            }) {
            };

            customRequest.setShouldCache(false);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(customRequest);

        } else {
            progressDialog.dismiss();
            opedialog("Connection Problem", "No Internet Connection !");
        }
    }
}
