package com.duainfotech.reggie.Activityy;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.duainfotech.reggie.R;

public class Main2Activity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout btnGotoApp, btnContact,btnLink,btnLink2;
    TextView lblTexting;
    final static int MY_PERMISSIONS_CODE = 1;
    public static Context con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main2);

        con = this;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
        } else {
            checkInterNetPremission();
        }

        btnGotoApp = (RelativeLayout) findViewById(R.id.btnGotoApp);
        btnContact = (RelativeLayout) findViewById(R.id.btnContact);
        btnLink = (RelativeLayout) findViewById(R.id.btnLink);
        btnLink2 = (RelativeLayout) findViewById(R.id.btnLink2);

        lblTexting = (TextView) findViewById(R.id.lblTexting);
        lblTexting.setText("\u25CF  Texting system & video email communication");

        btnContact.setOnClickListener(this);
        btnGotoApp.setOnClickListener(this);
        btnLink.setOnClickListener(this);
        btnLink2.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnGotoApp:
                startActivity(new Intent(Main2Activity.this, MainActivity.class));
                finish();
                break;
            case R.id.btnContact:
                startActivity(new Intent(Main2Activity.this, ContactMainActivity.class));
                finish();
                break;
            case R.id.btnLink:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.unitnet.com/ua/#/")));
                break;
            case R.id.btnLink2:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.yourpinkdelivery.com/")));
                break;
        }
    }

    public void checkInterNetPremission() {
        if (checkPermission()) {
        } else {
            requestPermission();
        }
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplication(), Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale((this), Manifest.permission.READ_EXTERNAL_STORAGE)) {

        } else {
            ActivityCompat.requestPermissions((this), new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                }
                return;
            }
        }
    }
}
