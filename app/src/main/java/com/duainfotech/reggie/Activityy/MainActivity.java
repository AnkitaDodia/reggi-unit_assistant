package com.duainfotech.reggie.Activityy;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.duainfotech.reggie.R;
import com.duainfotech.reggie.Utils.Api;
import com.duainfotech.reggie.Utils.CustomRequest;
import com.duainfotech.reggie.Utils.UtilsFile;
import com.yinglan.keyboard.HideUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends MasterActivity implements View.OnClickListener {

    RelativeLayout btnLogin, btnBack;

    TextInputLayout txtConsultantnumber;

    TextInputLayout txtPassword;

    public static Context con;

    public static ProgressDialog progressDialog;

    public static ArrayList<HashMap<String, String>> profiledetail;

    CheckBox checkbox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        HideUtil.init(this);

        con = this;

        txtConsultantnumber = (TextInputLayout) findViewById(R.id.txtConsultantnumber);
        txtPassword = (TextInputLayout) findViewById(R.id.txtPassword);

        checkbox = (CheckBox) findViewById(R.id.checkbox);

        //Code for check run time permission
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
        } else {
            //TODO
            deviceID = getIMEINumber();
            Log.e("IMEI_NUMBER" , deviceID);
        }

        if(getRemember())
        {
            checkbox.setChecked(true);
            txtConsultantnumber.getEditText().setText(getLoginId());
            txtPassword.getEditText().setText(getLoginPwd());
        }

//        txtConsultantnumber.getEditText().setText("G24093");
//        txtPassword.getEditText().setText("Dazzling1");

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Please Wait...!");
        progressDialog.setCancelable(false);

        btnBack = (RelativeLayout) findViewById(R.id.btnBack);
        btnLogin = (RelativeLayout) findViewById(R.id.btnLogin);

        btnBack.setOnClickListener(this);
        btnLogin.setOnClickListener(this);

        checkbox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    saveRememberMe(true);
                    saveLoginDetail(txtConsultantnumber.getEditText().getText().toString(),txtPassword.getEditText().getText().toString());
                }
                else
                {
                    saveRememberMe(false);
                }
            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            sendDeviceId();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                if(!txtConsultantnumber.getEditText().getText().toString().equalsIgnoreCase(getLoginId()) ||
                        !txtPassword.getEditText().getText().toString().equalsIgnoreCase(getLoginPwd()) && getRemember())
                {
                    saveLoginDetail(txtConsultantnumber.getEditText().getText().toString(),txtPassword.getEditText().getText().toString());
                }
                loginapicall();
                break;
            case R.id.btnBack:
                onBackPressed();
                break;
        }
    }

    private void loginapicall() {
        if (CustomRequest.hasConnection(this)) {

            progressDialog.show();

            String api = Api.baseurl + Api.login;

            Map<String, String> params = new HashMap<>();
            params.put("consultant_number", txtConsultantnumber.getEditText().getText().toString());
            params.put("password", txtPassword.getEditText().getText().toString());
            params.put("app", "a");
            params.put("version", getVersionInfo());

            Log.d("Params",params.toString());

            CustomRequest customRequest = new CustomRequest(Request.Method.POST, api, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("RESPONSE", "Login Responce==>" + response.toString());
                    try {

                        if(response.has("status") && response.getString("status").equals("1"))
                        {
                            sendDeviceId(); // Make api call for firebase notification

                            JSONObject jsonObject = response.getJSONObject("data");

                            profiledetail = new ArrayList<HashMap<String, String>>();

                            HashMap<String, String> map = new HashMap<String, String>();

                            map.put("Birthdate", jsonObject.getString("your_birthday"));
                            map.put("PhoneNumber", jsonObject.getString("admin_phone_number"));
                            map.put("Name", jsonObject.getString("name"));
                            map.put("Account", jsonObject.getString("account"));
                            map.put("AccountFull", jsonObject.getString("account_full"));
                            map.put("Routing", jsonObject.getString("routing"));
                            map.put("en", jsonObject.getString("en"));
                            map.put("sn", jsonObject.getString("sn"));
                            map.put("AdminUrl", jsonObject.getString("admin_email"));
                            map.put("IdClient", jsonObject.getString("id_client"));
                            map.put("CellPhoneNumber", jsonObject.getString("cell_phone_number"));
                            map.put("EmailAddress", jsonObject.getString("email_address"));

                            UtilsFile.clientID = jsonObject.getString("id_client");

                            UtilsFile.FTP_HOST = jsonObject.getString("ftp_host");
                            UtilsFile.FTP_USER = jsonObject.getString("ftp_user");
                            UtilsFile.FTP_PASS = jsonObject.getString("ftp_password");
                            UtilsFile.FTP_FILE_UPLOAD_PATH = jsonObject.getString("ftp_file_upload_path");

                            profiledetail.add(map);

                            progressDialog.dismiss();

                            startActivity(new Intent(MainActivity.this, HomeActivity.class));
                            finish();
                        }else {
                            progressDialog.dismiss();
                            if(response.getString("update").equalsIgnoreCase("1"))
                            {
                                openDialog(response.getString("message"));
                            }
                            else {
                                opedialog("Failed", response.getString("message"));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                }
            }) {
            };
            customRequest.setShouldCache(false);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(customRequest);
        } else {
            opedialog("Connection Problem", "No Internet Connection !");
        }
    }

    private void sendDeviceId()
    {
        if (CustomRequest.hasConnection(this)) {

            String api = Api.baseurl + Api.fireBase;

            Log.e("DEVICE_ID_IN_LOGIN",MasterActivity.deviceID);

            Map<String, String> params = new HashMap<>();
            params.put("id_fcm", getFCMID());
            params.put("id_device", MasterActivity.deviceID);
            params.put("device_type","a");

            Log.e("Params",params.toString());

            CustomRequest customRequest = new CustomRequest(Request.Method.POST, api, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("FROM_LOGIN", "FIRE_BASE_RESPONSE==>" + response.toString());
                    try {

                        if(response.has("status") && response.getString("status").equals("1"))
                        {
                            Log.e("message",response.getString("message"));
                        }else {
                            Log.e("message",response.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    Log.i("ERROR", "count==>" + error.toString());
                }
            }) {
            };
            customRequest.setShouldCache(false);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(customRequest);
        } else {
            opedialog("Connection Problem", "No Internet Connection !");
        }
    }

    public void opedialog(String title, String value) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(value).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
    }


    public void openDialog(String value) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setTitle("Update Application");
        alertDialogBuilder.setMessage(value).setCancelable(false)
                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        }
                        catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                }).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_PHONE_STATE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                    deviceID = getIMEINumber();
                    sendDeviceId();
                    Log.e("IMEI_NUMBER_OVERRIDE" , deviceID);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(MainActivity.this);
        myAlertDialog.setMessage("Are you sure you want to exit?");
        myAlertDialog.setTitle("Unit Assistant");
        myAlertDialog.setIcon(R.drawable.icon);
        myAlertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface arg0, int arg1) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                System.exit(1);
            }
        });
        myAlertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        myAlertDialog.show();
    }
}
