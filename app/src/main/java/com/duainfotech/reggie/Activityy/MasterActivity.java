package com.duainfotech.reggie.Activityy;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Ankita on 06-Jun-17.
 */
public class MasterActivity extends AppCompatActivity {
    public static boolean checkfile = false;
    public static String imageorpdf, deviceID,refreshedToken;

    public static ArrayList<File> filemapt = new ArrayList<>();
    public static ArrayList<String> ImageName = new ArrayList<>();

    public static final int REQUEST_READ_PHONE_STATE = 1;

    public void saveRememberMe(boolean flag) {
        SharedPreferences sp = getSharedPreferences("REMEMBER", MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putBoolean("REMEMBER_ME", flag);
        spe.commit();
    }

    public boolean getRemember() {
        SharedPreferences sp = getSharedPreferences("REMEMBER", MODE_PRIVATE);
        boolean flag = sp.getBoolean("REMEMBER_ME", false);
        return flag;
    }

    public void saveLoginDetail(String id, String pwd) {
        SharedPreferences sp = getSharedPreferences("LOGIN_Detail", MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("ID", id);
        spe.putString("PWD", pwd);
        spe.commit();
    }

    public String getLoginId() {
        SharedPreferences sp = getSharedPreferences("LOGIN_Detail", MODE_PRIVATE);
        String id = sp.getString("ID", "");
        return id;
    }

    public String getLoginPwd() {
        SharedPreferences sp = getSharedPreferences("LOGIN_Detail", MODE_PRIVATE);
        String pwd = sp.getString("PWD", "");
        return pwd;
    }

    public String getVersionInfo() {
        String versionName = "";
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return versionName;
    }

    public String getFCMID() {
        SharedPreferences sp = getSharedPreferences("FCM", MODE_PRIVATE);
        String flag = sp.getString("FCM_ID", "");
        return flag;
    }

    public String getIMEINumber() {
        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
//            return TODO;
        }
        String IMEINumber = mngr.getDeviceId();
        return IMEINumber;
    }
}
