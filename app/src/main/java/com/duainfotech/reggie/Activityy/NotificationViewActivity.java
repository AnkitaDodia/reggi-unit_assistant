package com.duainfotech.reggie.Activityy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.duainfotech.reggie.R;

/**
 * Created by My 7 on 19-Jan-18.
 */

public class NotificationViewActivity extends AppCompatActivity
{
    TextView notification_text,textview_title;

    RelativeLayout btnClose;

    String msg,title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_notification_viwe);

        btnClose = (RelativeLayout) findViewById(R.id.btnClose);

        notification_text = (TextView) findViewById(R.id.notification_text);
        textview_title = (TextView) findViewById(R.id.textview_title);

        msg = getIntent().getStringExtra("body");
        title = getIntent().getExtras().getString("title");

        textview_title.setText(title);
        notification_text.setText(msg);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(NotificationViewActivity.this,MainActivity.class);
                startActivity(it);
                finish();
            }
        });
    }
}
