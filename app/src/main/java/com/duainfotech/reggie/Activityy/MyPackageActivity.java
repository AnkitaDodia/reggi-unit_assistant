package com.duainfotech.reggie.Activityy;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.duainfotech.reggie.Adapterr.MyPackageListAdapter;
import com.duainfotech.reggie.R;
import com.duainfotech.reggie.Utils.Api;
import com.duainfotech.reggie.Utils.CustomRequest;
import com.duainfotech.reggie.Utils.UtilsFile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyPackageActivity extends AppCompatActivity implements View.OnClickListener {

    Context con;
    ListView listMyPackage;
    RelativeLayout btnBack;
    ProgressDialog progressDialog;

    public static ArrayList<HashMap<String, String>> MyPackageDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_my_package);

        con = this;
        listMyPackage = (ListView) findViewById(R.id.listMyPackage);
        btnBack = (RelativeLayout) findViewById(R.id.btnBack);

        progressDialog = new ProgressDialog(MyPackageActivity.this);
        progressDialog.setMessage("Loading...!");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        if (UtilsFile.clientID == null) {
            Log.e("ERROR", "NULL VALUE");
        } else {
            mypackageapicall();
        }

        btnBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void mypackageapicall() {
        if (CustomRequest.hasConnection(this)) {

            progressDialog.show();

            String api = Api.baseurl + Api.mypackageapi;

            Map<String, String> params = new HashMap<>();

            params.put("id_client", UtilsFile.clientID);

            CustomRequest customRequest = new CustomRequest(Request.Method.POST, api, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("RESPONSE", "count==>" + response.toString());
                    try {

                        String mstatus = response.getString("status");
                        if (mstatus.equals("1")) {

                            JSONArray jsonarray = response.getJSONArray("data");
                            MyPackageDetails = new ArrayList<HashMap<String, String>>();

                            for (int i = 0; i < jsonarray.length(); i++) {
                                JSONObject jsonObject = jsonarray.getJSONObject(i);
                                HashMap<String, String> map = new HashMap<String, String>();

                                map.put("Value", jsonObject.getString("value"));
                                map.put("Header", jsonObject.getString("header"));

                                MyPackageDetails.add(map);

                                if (i == jsonarray.length() - 1) {
                                    listMyPackage.setAdapter(new MyPackageListAdapter(con, MyPackageDetails));
                                    progressDialog.dismiss();
                                }
                            }
                        } else {
                            progressDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("ERROR", "count==>" + error.toString());
                    progressDialog.dismiss();

                    if (error.networkResponse != null) {
                        Log.e("ERROR", "Error Response code: " + error.networkResponse.statusCode);
                    }
                }
            }) {
            };

            customRequest.setShouldCache(false);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(customRequest);

        } else {
            progressDialog.dismiss();
            opedialog("Connection Problem", "No Internet Connection !");
        }
    }

    public void opedialog(String title, String value) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MyPackageActivity.this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(value).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
    }
}
