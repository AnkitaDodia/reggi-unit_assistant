package com.duainfotech.reggie.Activityy;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.duainfotech.reggie.R;
import com.duainfotech.reggie.Utils.Api;
import com.duainfotech.reggie.Utils.CustomRequest;
import com.duainfotech.reggie.Utils.UtilsFile;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.yinglan.keyboard.HideUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;

//import org.apache.commons.net.ftp.FTPClient;


public class UpdateNewsLetterActivity extends MasterActivity implements View.OnClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback {

    RelativeLayout btnBack, btnSave;
    Button btnUpload;
    EditText txtDesc;
    TextView lblDesc, lblNote;

    private static int RESULT_LOAD_IMG = 1;

    String imgDecodableString;

    public static ProgressDialog progressDialog;

    String newname = "";
    String strfilename = "";
    String NewFileName = "";

    File pdffile;
    FTPClient client;

    JSONParser mJSONParser;
    JSONArray filenamesArray;

    public static ArrayList<String> pdfname = new ArrayList<String>();
    public static ArrayList<String> pdfpath = new ArrayList<String>();

    private static final int REQUEST_PERMISSION = 1;
    private static String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_news_letter);

        HideUtil.init(this);

        verifyGalleryPermissions(UpdateNewsLetterActivity.this);

        lblDesc = (TextView) findViewById(R.id.lblDesc);
        lblNote = (TextView) findViewById(R.id.lblNote);

        btnBack = (RelativeLayout) findViewById(R.id.btnBack);
        btnSave = (RelativeLayout) findViewById(R.id.btnSave);
        btnUpload = (Button) findViewById(R.id.btnUpload);
        txtDesc = (EditText) findViewById(R.id.txtDesc);
        filenamesArray = new JSONArray();

        String notee = "<font color=#FF0000>*</font> <font color=#000000> Indicates mandatory fields.</font>";
        lblNote.setText(Html.fromHtml(notee));

        String desc = "<font color=#000000>Message </font> <font color=#FF0000>*</font> <font color=#000000>:</font>";
        lblDesc.setText(Html.fromHtml(desc));

        progressDialog = new ProgressDialog(UpdateNewsLetterActivity.this);
        progressDialog.setMessage("Loading...!");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        btnBack.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnUpload.setOnClickListener(this);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2 = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
            case R.id.btnSave:
                if (txtDesc.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(UpdateNewsLetterActivity.this, "Please enter your description!!", Toast.LENGTH_SHORT).show();
                } else {
                    changnewslatterapicall();
                }
                break;
            case R.id.btnUpload:
                uploadpic();
                break;
        }
    }

    public void opedialog(String title, String value) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(UpdateNewsLetterActivity.this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(value).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
    }

    private void uploadpic() {
        Intent it = new Intent(UpdateNewsLetterActivity.this, CustomGalleryActivity.class);
        startActivityForResult(it, 1001);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1001 && resultCode == RESULT_OK) {
            if (imageorpdf.equals("image")) {
                LinearLayout layout = (LinearLayout) findViewById(R.id.thumbnails);
                layout.removeAllViews();
                for (int i = 0; i < filemapt.size(); i++) {
                    ImageView imageView = new ImageView(this);

                    imageView.setId(i);
                    imageView.setPadding(2, 2, 2, 2);
                    String filePath = filemapt.get(i).getPath();
                    Log.e("filePath", "" + filePath);
                    Bitmap bitmap = BitmapFactory.decodeFile(filePath);
                    imageView.setImageBitmap(bitmap);
                    imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    layout.addView(imageView);


                    imageView.getLayoutParams().height = 150;
                    imageView.getLayoutParams().width = 150;
                    imageView.requestLayout();
                }
            }
        }
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("UpdateNewsLetter Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2.connect();
        AppIndex.AppIndexApi.start(client2, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client2, getIndexApiAction());
        client2.disconnect();
    }

    private class MyTransferListener implements FTPDataTransferListener {

        public void started() {
            Log.e("upload", "start");
        }

        public void transferred(int length) {
            Log.e("upload", "transfer");
        }

        public void completed() {
            Log.e("upload", "completed");

            strfilename = newname.substring(newname.lastIndexOf("/") + 1);

            Log.e("Old FILE NAME", "DuaUpload OLD Name===>" + strfilename);
            filenamesArray.put(strfilename);
            Toast.makeText(UpdateNewsLetterActivity.this, "Upload Completed!", Toast.LENGTH_LONG).show();

            changnewslatterapicall();
        }

        public void aborted() {
            Log.e("upload", "tryagain");
            opedialog("Failed", "File Upload Failed");
            Toast.makeText(UpdateNewsLetterActivity.this, "File Upload Failed!", Toast.LENGTH_LONG).show();

        }

        public void failed() {
            Log.e("upload", "failed....");
            opedialog("Failed", "File Upload Failed");
            Toast.makeText(UpdateNewsLetterActivity.this, "File Upload Failed!", Toast.LENGTH_LONG).show();
        }
    }

    private class JSONParser extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // Upload sdcard file
            if (imageorpdf.equals("image")) {
                for (int i = 0; i < filemapt.size(); i++) {
                    uploadFile(filemapt.get(i));
                }
            } else if (imageorpdf.equals("pdf")) {
                Log.e("PDFPATH", " ==> " + UtilsFile.PdfUri);
                pdffile = new File(String.valueOf(UtilsFile.PdfUri));

                uploadFile(pdffile);
            }

            return null;
        }

        protected void onPostExecute(String page) {
            changnewslatterapicall();
        }
    }

    private void uploadFile(File fileName) {

        strfilename = fileName.toString().substring(fileName.toString().lastIndexOf("/") + 1);

        Log.e("NAME", " ==> " + strfilename);

        client = new FTPClient();

        newname = fileName.toString();

        try {
            client.connect(UtilsFile.FTP_HOST, 21);
            client.login(UtilsFile.FTP_USER, UtilsFile.FTP_PASS);
            client.setType(FTPClient.TYPE_BINARY);

            Log.e("PATH", "==> " + UtilsFile.FTP_FILE_UPLOAD_PATH);

            client.changeDirectory(UtilsFile.FTP_FILE_UPLOAD_PATH);
            client.upload(fileName, 6000, new MyTransferListener());

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ERROR", "==> " + e);
            try {
                client.disconnect(true);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    private void changnewslatterapicall() {
        if (CustomRequest.hasConnection(this)) {
            progressDialog = new ProgressDialog(UpdateNewsLetterActivity.this);
            progressDialog.setMessage("Loading...!");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

                try {
                    upload(1,"https://unitassistant.website/api/apiChange.php");
                } catch (JSONException e) {
                    e.printStackTrace();
                }



        } else {
            progressDialog.dismiss();
            opedialog("Connection Problem", "No Internet Connection !");
        }
    }

    public static void verifyGalleryPermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity, PERMISSIONS, REQUEST_PERMISSION
            );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case REQUEST_PERMISSION:
                    uploadpic();
                    break;
            }
        } else {
            verifyGalleryPermissions(UpdateNewsLetterActivity.this);
        }
    }


    public void upload(final int imageIndex, final String url) throws JSONException
    {
        try
        {
            if(filemapt.size() == 0)
            {
                callSecondService();
            }
            else
            {
                Bitmap bm = BitmapFactory.decodeFile(filemapt.get(imageIndex-1).getAbsolutePath());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                byte[] b = baos.toByteArray();
                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("file",encodedImage);

                final int nextIndex = imageIndex + 1;

                Log.d("URL",""+url);
                Log.d("jsonObject",""+jsonObject.toString());

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("RESPONSE",response.toString());
                        try {
                            if (filenamesArray == null)
                                filenamesArray = new JSONArray();
                            filenamesArray.put(response.getString("file"));
                            if (nextIndex <= filemapt.size()) {
                                try {
                                    upload(nextIndex,url);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                //call second service
                                callSecondService();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.d("ERROR",error.toString());
                    }
                });

                jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                jsonObjectRequest.setShouldCache(false);
                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(jsonObjectRequest);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    public void callSecondService() {
        Log.d("FINISH","FINISH");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("requested_changes",txtDesc.getText().toString());
            jsonObject.put("files",filenamesArray);
            jsonObject.put("id_newsletter",UtilsFile.clientID);

            Log.e("PARAMS",jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("JSON RESPONSE",jsonObject.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "https://unitassistant.website/api/apiChangeAndriod.php", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("RESPONSE FROM 2nd",response.toString());

                try {
                    int status = response.getInt("status");
                    if (status == 1) {
                        android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(UpdateNewsLetterActivity.this);
                        alert.setTitle("Success");
                        alert.setMessage(response.getString("message"));
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                UpdateNewsLetterActivity.this.finish();
                                if(filemapt.size() != 0)
                                {
                                    filemapt.clear();
                                }
                            }
                        });
                        alert.show();
                    }else {
                        android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(UpdateNewsLetterActivity.this);
                        alert.setTitle("Error");
                        alert.setMessage(response.getString("message"));
                        alert.setPositiveButton("OK",null);
                        alert.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.d("ERROR",error.toString());
            }
        });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        jsonObjectRequest.setShouldCache(false);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }
}