package com.duainfotech.reggie.Activityy;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.duainfotech.reggie.R;
import com.duainfotech.reggie.Utils.Api;
import com.duainfotech.reggie.Utils.CustomRequest;
import com.duainfotech.reggie.Utils.UtilsFile;
import com.yinglan.keyboard.HideUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout btnBack, btnEdit, btnSave;
    EditText txtName, txtCellNumber, txtDob, txtAccount, txtRouting, txtEmail;
    ProgressDialog progressDialog;

    TextView lblName, lblCellNumber, lblDob, lblAccount, lblRouting, lblEmail, lblNote;

    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_profile);

        HideUtil.init(this);

        btnBack = (RelativeLayout) findViewById(R.id.btnBack);
        btnEdit = (RelativeLayout) findViewById(R.id.btnEdit);
        btnSave = (RelativeLayout) findViewById(R.id.btnSave);

        txtName = (EditText) findViewById(R.id.txtName);
        txtCellNumber = (EditText) findViewById(R.id.txtCellNumber);
        txtDob = (EditText) findViewById(R.id.txtDob);
        txtAccount = (EditText) findViewById(R.id.txtAccount);
        txtRouting = (EditText) findViewById(R.id.txtRouting);
        txtEmail = (EditText) findViewById(R.id.txtEmail);

        lblNote = (TextView) findViewById(R.id.lblNote);
        lblName = (TextView) findViewById(R.id.lblName);
        lblCellNumber = (TextView) findViewById(R.id.lblCellNumber);
        lblDob = (TextView) findViewById(R.id.lblDob);
        lblAccount = (TextView) findViewById(R.id.lblAccount);
        lblRouting = (TextView) findViewById(R.id.lblRouting);
        lblEmail = (TextView) findViewById(R.id.lblEmail);


        String notee = "<font color=#FF0000>*</font> <font color=#000000> Indicates mandatory fields.</font>";
        lblNote.setText(Html.fromHtml(notee));

        String name = "<font color=#000000>Name </font> <font color=#FF0000>*</font> <font color=#000000>:</font>";
        lblName.setText(Html.fromHtml(name));

        String cellno = "<font color=#000000>Cell Number </font> <font color=#FF0000>*</font> <font color=#000000>:</font>";
        lblCellNumber.setText(Html.fromHtml(cellno));

        String dob = "<font color=#000000>DOB </font> <font color=#FF0000>*</font> <font color=#000000>:</font>";
        lblDob.setText(Html.fromHtml(dob));

        String acc = "<font color=#000000>Account </font> <font color=#FF0000>*</font> <font color=#000000>:</font>";
        lblAccount.setText(Html.fromHtml(acc));

        String rout = "<font color=#000000>Routing </font> <font color=#FF0000>*</font> <font color=#000000>:</font>";
        lblRouting.setText(Html.fromHtml(rout));

        String email = "<font color=#000000>Email </font> <font color=#FF0000>*</font> <font color=#000000>:</font>";
        lblEmail.setText(Html.fromHtml(email));

        setdata();

        progressDialog = new ProgressDialog(ProfileActivity.this);
        progressDialog.setMessage("Please Wait...!");
        progressDialog.setCancelable(false);

        btnBack.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                if (btnSave.getVisibility() == View.VISIBLE) {
                    setdata();
                } else {
                    onBackPressed();
                }

                break;
            case R.id.btnSave:
                profilesaveapicall();
                break;
            case R.id.btnEdit:
                btnEdit.setVisibility(View.GONE);
                btnSave.setVisibility(View.VISIBLE);
                lblNote.setVisibility(View.VISIBLE);

                txtAccount.setText(MainActivity.profiledetail.get(0).get("AccountFull"));

                txtName.setEnabled(true);
                txtCellNumber.setEnabled(true);
                txtDob.setEnabled(true);
                txtAccount.setEnabled(true);
                txtRouting.setEnabled(true);
                txtEmail.setEnabled(true);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void opedialog(String title, String value) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileActivity.this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(value).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
    }


    private void profilesaveapicall() {
        if (CustomRequest.hasConnection(this)) {

            progressDialog.show();

            String api = Api.baseurl + Api.userprofile;

            Map<String, String> params = new HashMap<>();
            params.put("id_client", UtilsFile.clientID);
            params.put("name", txtName.getText().toString());
            params.put("cell_number", txtCellNumber.getText().toString());
            params.put("dob", txtDob.getText().toString());
            params.put("account", txtAccount.getText().toString());
            params.put("routing", txtRouting.getText().toString());
            params.put("email", txtEmail.getText().toString());

            CustomRequest customRequest = new CustomRequest(Request.Method.POST, api, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("RESPONSE", "Profile count==>" + response.toString());
                    try {

                        String mstatus = response.getString("status");
                        if (mstatus.equals("1")) {

                            progressDialog.dismiss();

                            opedialog("Success", response.getString("message"));

                            JSONObject jsonObject = response.getJSONObject("data");

                            MainActivity.profiledetail = new ArrayList<HashMap<String, String>>();

                            HashMap<String, String> map = new HashMap<String, String>();

                            map.put("Birthdate", jsonObject.getString("your_birthday"));
                            map.put("Name", jsonObject.getString("name"));
                            map.put("Account", jsonObject.getString("account"));
                            map.put("AccountFull", jsonObject.getString("account_full"));
                            map.put("Routing", jsonObject.getString("routing"));
                            map.put("CellPhoneNumber", jsonObject.getString("cell_phone_number"));
                            map.put("EmailAddress", jsonObject.getString("email_address"));

                            MainActivity.profiledetail.add(map);
//
                            Log.e("RESPONCEEEEEE", "Profile ==> " + MainActivity.profiledetail);

                            btnEdit.setVisibility(View.VISIBLE);
                            btnSave.setVisibility(View.GONE);
                            lblNote.setVisibility(View.GONE);

                            txtAccount.setText(MainActivity.profiledetail.get(0).get("Account"));

                            txtName.setEnabled(false);
                            txtCellNumber.setEnabled(false);
                            txtDob.setEnabled(false);
                            txtAccount.setEnabled(false);
                            txtRouting.setEnabled(false);
                            txtEmail.setEnabled(false);

                        } else {
                            progressDialog.dismiss();
                            opedialog("Failed", response.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    Log.i("ERROR", "count==>" + error.toString());
                }
            }) {
            };
            customRequest.setShouldCache(false);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(customRequest);
        } else {
            opedialog("Connection Problem", "No Internet Connection !");
        }
    }

    public void setdata() {
        id = MainActivity.profiledetail.get(0).get("IdClient");

        txtName.setText(MainActivity.profiledetail.get(0).get("Name"));
        txtDob.setText(MainActivity.profiledetail.get(0).get("Birthdate"));
        txtAccount.setText(MainActivity.profiledetail.get(0).get("Account"));
        txtCellNumber.setText(MainActivity.profiledetail.get(0).get("CellPhoneNumber"));
        txtRouting.setText(MainActivity.profiledetail.get(0).get("Routing"));
        txtEmail.setText(MainActivity.profiledetail.get(0).get("EmailAddress"));

        txtName.setEnabled(false);
        txtCellNumber.setEnabled(false);
        txtDob.setEnabled(false);
        txtAccount.setEnabled(false);
        txtRouting.setEnabled(false);
        txtEmail.setEnabled(false);

        lblNote.setVisibility(View.GONE);
        btnSave.setVisibility(View.GONE);
        btnEdit.setVisibility(View.VISIBLE);
    }
}
