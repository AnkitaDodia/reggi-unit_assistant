package com.duainfotech.reggie.Activityy;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.duainfotech.reggie.Adapterr.PdfListAdapter;
import com.duainfotech.reggie.R;

public class PdfListActivity extends AppCompatActivity implements View.OnClickListener {

    Context con;
    PdfListAdapter adapter;
    ListView listpdf;
    RelativeLayout btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pdf_list);

        con = this;
        listpdf = (ListView) findViewById(R.id.listpdf);

        adapter = new PdfListAdapter(con, UpdateNewsLetterActivity.pdfname, UpdateNewsLetterActivity.pdfpath);
        listpdf.setAdapter(adapter);

        btnBack = (RelativeLayout) findViewById(R.id.btnBack);

        btnBack.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        finish();
        Toast.makeText(this, "You haven't picked File", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
        }
    }
}
