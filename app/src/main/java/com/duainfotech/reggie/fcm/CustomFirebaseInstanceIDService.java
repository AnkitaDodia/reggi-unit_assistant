package com.duainfotech.reggie.fcm;

/**
 * Created by Adite-Ankita on 23-Aug-16.
 */

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.duainfotech.reggie.Activityy.HomeActivity;
import com.duainfotech.reggie.Activityy.MainActivity;
import com.duainfotech.reggie.Activityy.MasterActivity;
import com.duainfotech.reggie.Utils.Api;
import com.duainfotech.reggie.Utils.CustomRequest;
import com.duainfotech.reggie.Utils.UtilsFile;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CustomFirebaseInstanceIDService extends FirebaseInstanceIdService
{
    private static final String TAG = CustomFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        MasterActivity.refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Token_Value: " + MasterActivity.refreshedToken);

        saveFCMID(MasterActivity.refreshedToken);
        
        sendDeviceId(MasterActivity.refreshedToken);
    }

    private void sendDeviceId(String refreshedToken)
    {
        if (CustomRequest.hasConnection(this)) {

            String api = Api.baseurl + Api.fireBase;

            Map<String, String> params = new HashMap<>();
            params.put("id_fcm", refreshedToken);
            params.put("id_device ", MasterActivity.deviceID);
            params.put("device_type","a");

            Log.d("Params",params.toString());

            CustomRequest customRequest = new CustomRequest(Request.Method.POST, api, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("RESPONSE", "FIRE_BASE_RESPONSE==>" + response.toString());
                    try {

                        if(response.has("status") && response.getString("status").equals("1"))
                        {
                            Log.e("FIRE_BASE_RESPONSE",response.getString("message"));
                        }else {
                            Log.e("FIRE_BASE_RESPONSE",response.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    Log.i("ERROR", "count==>" + error.toString());
                }
            }) {
            };
            customRequest.setShouldCache(false);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(customRequest);
        } else {
            opedialog("Connection Problem", "No Internet Connection !");
        }
    }

    public void opedialog(String title, String value) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CustomFirebaseInstanceIDService.this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(value).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
    }

    public void saveFCMID(String id) {
        SharedPreferences sp = getSharedPreferences("FCM", MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("FCM_ID", id);
        spe.commit();
    }
}