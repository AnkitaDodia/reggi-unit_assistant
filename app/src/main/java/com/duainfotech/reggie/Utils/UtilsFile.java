package com.duainfotech.reggie.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by duainfotech on 1/5/17.
 */
public class UtilsFile {

    public static String clientID;

    public static Bitmap image;

    public static Uri PdfUri;

    public static final String SettingsBooks = "REGGIE";

    //database and sharedpreference
    public static SharedPreferences Preference;
    public static SharedPreferences.Editor editor;


    public static void createPref(Context con) {
        Preference = con.getSharedPreferences(UtilsFile.SettingsBooks, 0);
        editor = Preference.edit();
    }

    /*********
     * FTP HOST
     ***********/
    public static String FTP_HOST;

    /*********
     * FTP USERNAME
     ***********/
    public static String FTP_USER;

    /*********
     * FTP PASSWORD
     ***********/
    public static String FTP_PASS;

    /*********
     * FTP FILE UPLOAD PATH
     ***********/
//    public static String FTP_FILE_UPLOAD_PATH = "/upload/";
    public static String FTP_FILE_UPLOAD_PATH;
}
