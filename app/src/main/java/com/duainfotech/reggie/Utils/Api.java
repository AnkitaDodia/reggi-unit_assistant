package com.duainfotech.reggie.Utils;

/**
 * Created by duainfotech on 1/5/17.
 */
public class Api {

        public static String baseurl = "https://unitassistant.website/api/";
//    public static String baseurl = "http://unitas.aasthasolutions.com/api/";

    public static String login = "apiUserLogin.php";
    public static String changnewslatter = "apiNewsletterChanges.php";
    public static String registertoken = "apiResponseToken.php";
    public static String userprofile = "apiUserProfileSave.php";
    public static String contactapi = "apiContact.php";
    public static String mypackageapi = "apiClientPackage.php";
    public static String bitlyapi = "apiBitly.php";
    public static String fireBase = "apiDevice.php";
    public static String consultantPacket = "apiNcp.php";

    //new API
    public static String approvenewslettereapi = "apiNewsletterApprove.php";

}
